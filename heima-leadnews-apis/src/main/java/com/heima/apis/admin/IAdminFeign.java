package com.heima.apis.admin;

import com.heima.model.admin.pojos.AdChannel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient("leadnews-admin")
public interface IAdminFeign {


    @GetMapping("/api/v1/channel/listAll")
    List<AdChannel> listAll();
}
