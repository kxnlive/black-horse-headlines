package com.heima.apis.user;

import com.heima.model.user.pojos.ApUser;
import com.heima.model.user.pojos.ApUserFollow;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("leadnews-user")
public interface IUserFeign {


    @GetMapping("/api/v1/user_follow/one")
    ApUserFollow findOneUserFollow(@RequestParam Integer userId, @RequestParam Integer followId);


    @GetMapping("/api/v1/user/{id}")
    ApUser findById(@PathVariable("id") Integer id);
}
