package com.heima.apis.behavior;


import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApLikesBehavior;
import com.heima.model.behavior.pojos.ApUnlikesBehavior;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("leadnews-behavior")
public interface IBehaviorFeign {


    @GetMapping("/api/v1/findByUserIdAndEquipmentId")
    ApBehaviorEntry findByUserIdAndEquipmentId(@RequestParam(value = "userId", required = false) Integer userId,
                                               @RequestParam(value = "equipmentId", required = false) Integer equipmentId);


    @GetMapping("/api/v1/likes_behavior/one")
    ApLikesBehavior findOneLikesBehavior(@RequestParam Long articleId, @RequestParam Integer entryId,@RequestParam Short type);


    @GetMapping("/api/v1/un_likes_behavior/one")
    ApUnlikesBehavior findOneUnlikesBehavior(@RequestParam Long articleId, @RequestParam Integer entryId);
}
