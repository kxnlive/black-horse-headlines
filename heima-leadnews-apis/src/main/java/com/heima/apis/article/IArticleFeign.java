package com.heima.apis.article;

import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("leadnews-article")
public interface IArticleFeign {


    /**
     * 根据用户ID查询作者
     * @param userId
     * @return
     */
    @GetMapping("/api/v1/author/findByUserId/{userId}")
    ApAuthor findByUserId(@PathVariable("userId") Integer userId);


    /**
     * 保存作者
     * @param apAuthor
     * @return
     */
    @PostMapping("/api/v1/author/save")
    ResponseResult save(@RequestBody ApAuthor apAuthor);


    /**
     * 根据自媒体用户ID查询作者
     * @param wmUserId
     * @return
     */
    @GetMapping("/api/v1/author/findByWmUserId/{wmUserId}")
    ApAuthor findByWmUserId(@PathVariable("wmUserId") Integer wmUserId);


    /**
     * 保存APP文章等信息
     * @param dto
     * @return
     */
    @PostMapping("/api/v1/article/save")
    ResponseResult saveApArticle(@RequestBody ArticleDto dto);

    /**
     * 根据作者ID查询作者
     * @param id
     * @return
     */
    @GetMapping("/api/v1/author/one/{id}")
    ApAuthor findAuthorById(@PathVariable("id") Integer id);
}
