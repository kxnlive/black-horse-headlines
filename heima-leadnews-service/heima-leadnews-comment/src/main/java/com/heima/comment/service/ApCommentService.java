package com.heima.comment.service;

import com.heima.model.comment.dtos.CommentDto;
import com.heima.model.comment.dtos.CommentLikeDto;
import com.heima.model.comment.dtos.CommentSaveDto;
import com.heima.model.common.dtos.ResponseResult;

public interface ApCommentService {

    /**
     * 发布评论
     * @param dto
     * @return
     */
    ResponseResult saveComment(CommentSaveDto dto);


    /**
     * 评论点赞或取消点赞
     * @param dto
     * @return
     */
    ResponseResult like(CommentLikeDto dto);


    /**
     * 评论列表查询
     * @param dto
     * @return
     */
    ResponseResult load(CommentDto dto);
}
