package com.heima.comment.service.impl;

import com.heima.comment.service.ApCommentHotService;
import com.heima.model.comment.pojos.ApComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ApCommentHotServiceImpl implements ApCommentHotService {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 计算热点评论的异步任务
     * @param apComment
     */
    @Async("taskExecutor")
    @Override
    public void computeHotComment(ApComment apComment) {

        //1.查询所有热点评论集合（查询条件：文章ID、热点的   结果：点赞数倒排）
        Query query = Query.query(Criteria.where("objectId").is(apComment.getObjectId()).and("flag").is(1)).with(Sort.by(Sort.Direction.DESC, "likes"));
        List<ApComment> apCommentList = mongoTemplate.find(query, ApComment.class);

        if(apCommentList==null || apCommentList.size()<5){
            //2.判断集合的元素个数，如果小于5，则直接将当前评论更新为热点评论并保存

            apComment.setFlag((short)1);//设置为热点评论
            apComment.setUpdatedTime(new Date());
            mongoTemplate.save(apComment);//更新评论
        } else {
            //3.否则，当前评论就与列表中最后一条评论的点赞数做对比

            //获取列表中最后一条评论
            ApComment apCommentLast = apCommentList.get(apCommentList.size() - 1);
            if(apComment.getLikes()>apCommentLast.getLikes()){
                apCommentLast.setFlag((short)0); //设置为普通评论
                apCommentLast.setUpdatedTime(new Date()); //更新时间
                mongoTemplate.save(apCommentLast);

                apComment.setFlag((short)1);//设置为热点评论
                apComment.setUpdatedTime(new Date());
                mongoTemplate.save(apComment);//更新评论

            }
        }
    }
}
