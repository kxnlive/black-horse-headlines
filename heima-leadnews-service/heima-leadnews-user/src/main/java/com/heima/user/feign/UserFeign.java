package com.heima.user.feign;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.apis.user.IUserFeign;
import com.heima.model.user.pojos.ApUser;
import com.heima.model.user.pojos.ApUserFollow;
import com.heima.user.mapper.ApUserFollowMapper;
import com.heima.user.mapper.ApUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserFeign implements IUserFeign {

    @Autowired
    private ApUserFollowMapper apUserFollowMapper;

    @Autowired
    private ApUserMapper apUserMapper;

    @Override
    @GetMapping("/api/v1/user_follow/one")
    public ApUserFollow findOneUserFollow(@RequestParam Integer userId, @RequestParam Integer followId){
        return apUserFollowMapper.selectOne(Wrappers.<ApUserFollow>lambdaQuery()
        .eq(ApUserFollow::getUserId, userId)
        .eq(ApUserFollow::getFollowId, followId));
    }


    @Override
    @GetMapping("/api/v1/user/{id}")
    public ApUser findById(@PathVariable("id") Integer id){
        return apUserMapper.selectById(id);
    }
}
