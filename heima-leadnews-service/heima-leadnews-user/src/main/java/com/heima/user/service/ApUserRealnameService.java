package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.AuthDto;
import com.heima.model.user.pojos.ApUserRealname;

public interface ApUserRealnameService extends IService<ApUserRealname> {

    /**
     * 用户认证信息分页列表查询
     * @param dto
     * @return
     */
    ResponseResult list(AuthDto dto);


    /**
     * 用户认证信息审核
     * @param dto
     * @param status  2-审核驳回  9-审核通过
     * @return
     */
    ResponseResult auth(AuthDto dto,Short status);
}
