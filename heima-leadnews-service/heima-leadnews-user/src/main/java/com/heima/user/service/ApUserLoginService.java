package com.heima.user.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDto;

public interface ApUserLoginService {


    /**
     * app端用户登录（正常登录模式和游客模式）
     * @param dto
     * @return
     */
    ResponseResult login(LoginDto dto);
}
