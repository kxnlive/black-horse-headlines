package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.apis.article.IArticleFeign;
import com.heima.apis.wemedia.IWemediaFeign;
import com.heima.common.constants.user.UserConstants;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.AuthDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.model.user.pojos.ApUserRealname;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.mapper.ApUserRealnameMapper;
import com.heima.user.service.ApUserRealnameService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Transactional
@Service
public class ApUserRealnameServiceImpl extends ServiceImpl<ApUserRealnameMapper, ApUserRealname> implements ApUserRealnameService {



    @Override
    public ResponseResult list(AuthDto dto) {
        //1.判断请求参数是否为空
        if(dto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //2.检查分页参数并设置默认值
        dto.checkParam();

        //3.判断dto里status如果有值就拼接查询条件
        LambdaQueryWrapper<ApUserRealname> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if(dto.getStatus()!=null){
            lambdaQueryWrapper.eq(ApUserRealname::getStatus, dto.getStatus());
        }

        //4.执行分页查询
        IPage<ApUserRealname> page = new Page<>(dto.getPage(), dto.getSize());
        this.page(page,lambdaQueryWrapper);

        //5.封装响应结果
        ResponseResult responseResult = new PageResponseResult(dto.getPage(),dto.getSize(),(int)page.getTotal());
        responseResult.setData(page.getRecords());

        //6.响应数据
        return responseResult;
    }

    @Autowired
    private ApUserMapper apUserMapper;


    @GlobalTransactional
    @Override
    public ResponseResult auth(AuthDto dto, Short status) {
        //第一部分：准入校验
        //1.1 判断dto参数是否合法
        if(dto==null || dto.getId()==null || dto.getId()==0 || status==null ){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //1.2 根据ID查询认证信息判断是否存在
        ApUserRealname apUserRealname = getById(dto.getId());
        if(apUserRealname==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "认证信息不存在");
        }

        //1.3 根据USER_ID查询AppUser并判断是否存在
        ApUser apUser = apUserMapper.selectById(apUserRealname.getUserId());
        if(apUser==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "app用户不存在");
        }

        //1.4 判断如果已经有审核结果则响应重复审核
        if(apUserRealname.getStatus().equals(UserConstants.FAIL_AUTH) ||  apUserRealname.getStatus().equals(UserConstants.PASS_AUTH)){
            return ResponseResult.errorResult(AppHttpCodeEnum.LOGIC_ERROR, "不要重复审核");
        }


        //第二部分：修改用户认证状态
        //2.1 设置要修改的值
        ApUserRealname apUserRealnameDB = new ApUserRealname();
        apUserRealnameDB.setId(dto.getId()); //修改的条件
        apUserRealnameDB.setStatus(status);
        if(StringUtils.isNotBlank(dto.getMsg())){
            apUserRealnameDB.setReason(dto.getMsg()); //设置拒绝原因
        }

        //2.2 根据ID修改
        updateById(apUserRealnameDB);


        //当前是审核通过时，执行第三部分：创建自主媒体用户 + 创建作者 + 修改ApUser的标识
        if(status.equals(UserConstants.PASS_AUTH)){

            ResponseResult responseResult = createWmUserAndApAuthor(dto, apUser);
            if(responseResult!=null){
                return responseResult;
            }
        }

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Autowired
    private IWemediaFeign wemediaFeign;

    @Autowired
    private IArticleFeign articleFeign;

    /**
     * 创建自主媒体用户 + 创建作者 + 修改ApUser的标识
     * @param dto
     * @param apUser
     * @return
     */
    private ResponseResult createWmUserAndApAuthor(AuthDto dto, ApUser apUser) {

        //******************创建自主媒体用户***************************
        //调用feign接口查询自媒体用户
        WmUser wmUser = wemediaFeign.findByName(apUser.getName());

        //判断如果自媒体用户存在，则响应重复创建
        if(wmUser!=null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST, "自媒体用户已创建过，无需重复创建");
        }

        //如果不存在，就调用feign接口创建
        wmUser = new WmUser();
        wmUser.setApUserId(apUser.getId()); //app用户ID
        wmUser.setName(apUser.getName());//app用户登录名
        wmUser.setPassword(apUser.getPassword());//app用户登录密码
        wmUser.setPhone(apUser.getPhone());//手机号
        wmUser.setImage(apUser.getImage()); //头像
        wmUser.setType(0);//个人用户
        wmUser.setStatus(9); //正常可用状态
        wmUser.setCreatedTime(new Date());
        wemediaFeign.save(wmUser);


        //******************创建作者***************************
        //调用feign接口查询作者
        ApAuthor apAuthor = articleFeign.findByUserId(apUser.getId());

        //判断如果作者存在，则响应重复创建
        if(apAuthor!=null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST, "作者已创建过，无需重复创建");
        }

        //如果作者不存在，调用feign接口创建
        apAuthor = new ApAuthor();
        apAuthor.setUserId(apUser.getId());//ap用户ID
        //查询刚创建完的wmUser获取其id
        wmUser = wemediaFeign.findByName(apUser.getName());
        apAuthor.setWmUserId(wmUser.getId());//自媒体用户ID
        apAuthor.setName(apUser.getName());
        apAuthor.setType(2);//自媒体人
        apAuthor.setCreatedTime(new Date());
        articleFeign.save(apAuthor);

        //TODO 更新wmUser的apAuthorId

        //******************修改ApUser的标识***************************
        ApUser apUserDB = new ApUser();
        apUserDB.setId(apUser.getId());
        apUserDB.setFlag((short)1);//自媒体人
        apUserMapper.updateById(apUserDB);

        int i = 1/0;
        return null;
    }
}
