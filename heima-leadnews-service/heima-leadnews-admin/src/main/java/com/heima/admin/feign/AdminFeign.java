package com.heima.admin.feign;

import com.heima.admin.service.AdChannelService;
import com.heima.apis.admin.IAdminFeign;
import com.heima.model.admin.pojos.AdChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AdminFeign implements IAdminFeign {

    @Autowired
    private AdChannelService adChannelService;

    @Override
    @GetMapping("/api/v1/channel/listAll")
    public List<AdChannel> listAll() {
        return adChannelService.list();
    }
}
