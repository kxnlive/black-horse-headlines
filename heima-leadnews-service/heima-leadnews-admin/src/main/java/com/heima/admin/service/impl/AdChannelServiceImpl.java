package com.heima.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.admin.mapper.AdChannelMapper;
import com.heima.admin.service.AdChannelService;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Transactional
@Service
public class AdChannelServiceImpl extends ServiceImpl<AdChannelMapper, AdChannel> implements AdChannelService {


    /**
     * 分页查询频道列表
     * @param dto 请求参数
     * @return
     */
    @Override
    public ResponseResult list(ChannelDto dto) {
        //1.判断参数是否为空
        if(dto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //2.判断请求参数并设置默认值
        dto.checkParam();

        LambdaQueryWrapper<AdChannel> lambdaQueryWrapper = new LambdaQueryWrapper();
        //3.判断请求参数中频道名是否有值，如果有值就拼接模糊查询
        if(StringUtils.isNotBlank(dto.getName())){
            lambdaQueryWrapper.like(AdChannel::getName,dto.getName());
        }


        IPage<AdChannel> page = new Page<>(dto.getPage(), dto.getSize());
        //4.执行分页查询
        page(page, lambdaQueryWrapper);

        //5.封装分页响应结果
        ResponseResult responseResult = new PageResponseResult(dto.getPage(),dto.getSize(),(int)page.getTotal());
        responseResult.setData(page.getRecords());

        //6.响应数据
        return responseResult;
    }


    /**
     * 新增频道
     * @param adChannel
     * @return
     */
    @Override
    public ResponseResult insert(AdChannel adChannel) {
        //1.判断参数是否为空
        if(adChannel==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //2.设置必要字段的默认值
        adChannel.setIsDefault(false); //非默认
        adChannel.setCreatedTime(new Date()); //创建时间

        //3.执行保存
        save(adChannel);

        //4.响应数据
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


    /**
     * 更新频道
     * @param adChannel
     * @return
     */
    @Override
    public ResponseResult update(AdChannel adChannel) {
        //1.判断参数是否为空
        if(adChannel==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //2.根据ID查询频道并判断是否为空
        AdChannel channel = getById(adChannel.getId());
        if(channel==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "频道不存在");
        }

        //3.根据ID更新频道
        updateById(adChannel);

        //4.响应数据
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


    /**
     * 删除频道
     * @param id
     * @return
     */
    @Override
    public ResponseResult del(Integer id) {
        //1.判断参数是否为空
        if(id==null || id<=0 ){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //2.根据ID查询频道并判断是否存在
        AdChannel channel = getById(id);
        if(channel==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "频道不存在");
        }

        //3.判断频道是否启用
        if(channel.getStatus()){
            return ResponseResult.errorResult(AppHttpCodeEnum.LOGIC_ERROR, "频道已启动不能删除");
        }

        //4.根据ID删除频道
        removeById(id);

        //5.响应数据
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
