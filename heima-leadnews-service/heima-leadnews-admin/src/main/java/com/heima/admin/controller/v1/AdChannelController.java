package com.heima.admin.controller.v1;


import com.heima.admin.service.AdChannelService;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "频道管理接口", tags = "channel", description = "频道管理接口")
@RestController
@RequestMapping("/api/v1/channel")
public class AdChannelController {

    @Autowired
    private AdChannelService adChannelService;

    @ApiOperation(value = "频道分页查询接口")
    @PostMapping("/list")
    public ResponseResult list(@RequestBody ChannelDto dto){

        return adChannelService.list(dto);
    }

    @ApiOperation(value = "新增频道接口")
    @PostMapping("/save")
    public ResponseResult insert(@RequestBody AdChannel adChannel){
        return adChannelService.insert(adChannel);
    }

    @ApiOperation(value = "更新频道接口")
    @PostMapping("/update")
    public ResponseResult update(@RequestBody AdChannel adChannel){
        return adChannelService.update(adChannel);
    }

    @ApiOperation(value = "删除频道接口")
    @GetMapping("/del/{id}")
    public ResponseResult del(@PathVariable("id") Integer id){
        return adChannelService.del(id);
    }


    @ApiOperation(value = "查询全部频道接口")
    @GetMapping("/channels")
    public ResponseResult listAll(){
        return ResponseResult.okResult(adChannelService.list());
    }


}
