package com.heima.admin.task;


import com.heima.admin.service.WmNewsAutoScanService;
import com.heima.apis.wemedia.IWemediaFeign;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 自媒体文章定时审核发布的任务
 */
@Component
@Slf4j
public class WmNewsAutoPublishTask {

    @Autowired
    private IWemediaFeign wemediaFeign;

    @Autowired
    private WmNewsAutoScanService wmNewsAutoScanService;

    /**
     * 调度任务的方法
     */
    @XxlJob("wmNewsAutoPulishJobHandler")
    public ReturnT<String> demoJobHandler(String param) throws Exception {

        //1.调用feign获取所有待发布文章（状态为4或8，且发布时间小于系统当前时间）的ID集合
        List<Integer> idList = wemediaFeign.findRelease();
        if(idList.size()>0){
            log.info("[定时审核]定时审核文章开始..............");
            //2.遍历文章ID，逐一调用自动审核
            for (Integer newsId : idList) {
                wmNewsAutoScanService.autoScanWmNewsById(newsId);
            }
            log.info("[定时审核]定时审核文章结束.............." );
        } else {
            log.info("[定时审核]暂无待审核的文章");
        }
        return ReturnT.SUCCESS;
    }
}
