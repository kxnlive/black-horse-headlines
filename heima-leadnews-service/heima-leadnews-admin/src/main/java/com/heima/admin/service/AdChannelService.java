package com.heima.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.admin.dtos.ChannelDto;
import com.heima.model.admin.pojos.AdChannel;
import com.heima.model.common.dtos.ResponseResult;

public interface AdChannelService extends IService<AdChannel> {

    /**
     * 分页查询频道列表
     * @param dto
     * @return
     */
    ResponseResult list(ChannelDto dto);


    /**
     * 新增频道
     * @param adChannel
     * @return
     */
    ResponseResult insert(AdChannel adChannel);


    /**
     * 更新频道
     * @param adChannel
     * @return
     */
    ResponseResult update(AdChannel adChannel);

    /**
     * 删除频道
     * @param id
     * @return
     */
    ResponseResult del(Integer id);
}
