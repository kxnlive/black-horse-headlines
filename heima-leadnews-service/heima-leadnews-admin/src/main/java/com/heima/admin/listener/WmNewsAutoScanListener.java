package com.heima.admin.listener;

import com.heima.admin.service.WmNewsAutoScanService;
import com.heima.common.constants.admin.NewsAutoScanConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Kafka消费者监听器，负责调用文章自动审核逻辑
 */
@Component
@Slf4j
public class WmNewsAutoScanListener {

    @Autowired
    private WmNewsAutoScanService wmNewsAutoScanService;


    @KafkaListener(topics = NewsAutoScanConstants.WM_NEWS_AUTO_SCAN_TOPIC)
    public void msgReceive(ConsumerRecord<String,String> consumerRecord){
        Optional<ConsumerRecord<String, String>> optional = Optional.ofNullable(consumerRecord);
        optional.ifPresent( x -> {
            Integer wmNewsId = Integer.valueOf(x.value());
            log.info("[自动审核MQ监听器]开始执行自动审核，文章ID：{}.....", wmNewsId);
            wmNewsAutoScanService.autoScanWmNewsById(wmNewsId);
            log.info("[自动审核MQ监听器]完成自动审核，文章ID：{}.....", wmNewsId);
        });
    }
}
