package com.heima.admin.service;

public interface WmNewsAutoScanService {

    /**
     * 根据自媒体文章ID完成自动审核
     * @param id
     */
    void autoScanWmNewsById(Integer id);
}
