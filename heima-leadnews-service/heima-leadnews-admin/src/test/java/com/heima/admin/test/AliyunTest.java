package com.heima.admin.test;


import com.heima.admin.AdminApplication;
import com.heima.common.aliyun.GreenImageScan;
import com.heima.common.aliyun.GreenTextScan;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Map;

/**
 * 阿里云内容安全检测测试
 */
@SpringBootTest(classes = AdminApplication.class)
@RunWith(SpringRunner.class)
public class AliyunTest {

    @Autowired
    private GreenTextScan greenTextScan;


    /**
     * 测试文本内容检测
     */
    @Test
    public void testTextScan(){
        try {
            Map<String,String> map = greenTextScan.greeTextScan("买卖图书是非法的");
            String suggestion = map.get("suggestion"); //获取阿里云响应结果建议
            if(suggestion.equals("block")){
                System.out.println("内容违规，建议审核拒绝");
            } else if(suggestion.equals("review")){
                System.out.println("内容有不确定因素，建议人工审核");
            } else {
                System.out.println("内容安全，建议审核通过");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Autowired
    private GreenImageScan greenImageScan;

    /**
     * 测试图片内容审核
     */
    @Test
    public void testImageScan(){
        try {
            Map<String,String> map = greenImageScan.imageScan(Collections.singletonList("https://hmleadnews376.oss-cn-beijing.aliyuncs.com/material/2021/6/20210613/2816abe1ef3549a48383ad20b21c110d.png"));
            String suggestion = map.get("suggestion"); //获取阿里云响应结果建议
            if(suggestion.equals("block")){
                System.out.println("内容违规，建议审核拒绝");
            } else if(suggestion.equals("review")){
                System.out.println("内容有不确定因素，建议人工审核");
            } else {
                System.out.println("内容安全，建议审核通过");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
