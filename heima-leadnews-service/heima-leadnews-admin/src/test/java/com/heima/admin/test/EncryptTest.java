package com.heima.admin.test;

import com.heima.utils.common.BCrypt;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.springframework.util.DigestUtils;

/**
 * 加密算法测试
 */
public class EncryptTest {


    /**
     * md5不加盐直接加密
     */
    @Test
    public void testMd5(){
        for (int i = 0; i < 10; i++) {
            String pwd = "123456";  //明文密码
            String pwdEnrypt = DigestUtils.md5DigestAsHex(pwd.getBytes());//密文密码
            System.out.println("密文密码：" +  pwdEnrypt);
        }
    }


    /**
     * 测试md5+盐生成（应用场景：一般用户注册或新增用户保存用户密码）
     */
    @Test
    public void testMd5SaltForRegister(){
        for (int i = 0; i < 10; i++) {
            String pwd = "123456"; //明文密码
            String salt = RandomStringUtils.randomAlphanumeric(10); //随机数
            String pwdSalt = pwd + salt;
            String pwdEncrypt = DigestUtils.md5DigestAsHex(pwdSalt.getBytes());//密文密码
            System.out.println("salt:" + salt +", pwdEncrypt:" + pwdEncrypt);
        }
    }

    /**
     * 测试md5+盐验证（应用场景：一般用于用户登录校验密码是否正确）
     */
    @Test
    public void testMd5SaltForLogin(){
        String dbSalt = "DvntPbrYsT"; //数据库中用户的盐
        String dbPwd = "8b5612d1bebddeff613eaf1366f4681c"; //数据库中用户的密文密码

        String loginPwd = "123456"; //用户明文密码

        //对比密码
        //1.用户登录密码+salt，得到新密码
        String loginPwdNew = loginPwd + dbSalt;

        //2.对拼接后的密码进行md5运算得到密文
        String pwdEnrypt = DigestUtils.md5DigestAsHex(loginPwdNew.getBytes());//密码密文

        //3.将密码密文与数据库密码进行对比
        if(pwdEnrypt.equals(dbPwd)){//4.对比一致就密码正确
            System.out.println("密码正确");
        } else {//5.否则就密码错误
            System.out.println("密码错误");
        }
    }

    /**
     * 测试bcrypt生成密码（应用场景：一般用户注册或新增用户保存用户密码）
     */
    @Test
    public void testBcryptForRegister(){
        for (int i = 0; i < 10; i++) {
            String pwd = "123456"; //明文密码
            String salt = BCrypt.gensalt(); //随机盐
            String pwdEncrypt = BCrypt.hashpw(pwd, salt); //密文密码
            System.out.println("salt:" + salt +", pwdEncrypt:" + pwdEncrypt);
        }
    }

    /**
     * 测试bcrypt验证密码（应用场景：一般用于用户登录校验密码是否正确）
     */
    @Test
    public void testBcryptForLogin(){
        String loginPwd = "123456"; //用户登录密码
        String dbPwd = "$2a$10$Ho2yJcZRg79.V6wotVeo2OqbHrk2sQ9Xc6ClAyAdzHxuZ2ez6k48e"; //数据库中用户的密文密码
        boolean flag = BCrypt.checkpw(loginPwd, dbPwd);
        if(flag){
            System.out.println("密码正确");
        } else {
            System.out.println("密码错误");
        }
    }
}
