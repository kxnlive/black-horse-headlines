package com.heima.admin.test;

import com.heima.admin.AdminApplication;
import com.heima.admin.service.WmNewsAutoScanService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = AdminApplication.class)
@RunWith(SpringRunner.class)
public class WmNewsTest {

    @Autowired
    private WmNewsAutoScanService autoScanService;


    @Test
    public void testAutoScan(){

        //场景6：文章的文本和图片全部正常，但时间不允许发布
        autoScanService.autoScanWmNewsById(6239);
    }
}
