package com.heima.admin.test;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.Test;

import java.util.Date;
import java.util.UUID;

/**
 * jwt创建与解析测试
 */
public class JwtTest {


    /**
     * 测试生成jwt令牌
     *
     * eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI2NGIwODgxOC0zNWZjLTQ2NzUtYWVlNS1iZGJjMjE5NDY1Y2EiLCJzdWIiOiJjbGllbnQiLCJ1c2VybmFtZSI6InpoYW5nc2FuIiwiYWdlIjoyMH0.4L1rFL6iF-HkdNMZCJ99Wha-tjj_qEFyWdJh4H4CjD-Jp1msVFUVKgqywYE4Qj6D2jqPPULS2szGQLFHxOb-nA
     * eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI2MTJlY2Q1ZC1hZjBkLTQ0NzktODc1My1kOTdkNmUwZmJmY2EiLCJzdWIiOiJjbGllbnQiLCJ1c2VybmFtZSI6InpoYW5nc2FuIiwiYWdlIjoyMH0.lbmMssPg6omk4iNJwRg9Q_7_BC3a7ZYnXilESImPvLjuP2kVVZDxkT6MTVxC2pX82R5XItWlYMd5r7KZhFS0XQ
     */
    @Test
    public void testJwtCreate(){
        String token = Jwts.builder().signWith(SignatureAlgorithm.HS512, "itcast")
                .setId(UUID.randomUUID().toString()) //jwt唯一标识
                .setIssuedAt(new Date()) //jwt生成时间
                .setExpiration(new Date(System.currentTimeMillis()+60*60*1000)) //jwt过期时间
                .setSubject("client") //主题
                .claim("username", "zhangsan") //自定义属性
                .claim("age", 20) //自定义属性
                .compact();
        System.out.println("token:" + token);
    }


    /**
     * 测试验证jwt令牌
     * 如何防止篡改：解析时根据第一部分内的加密算法，使用密钥对前两部分值生成新的签名，将新签名与第三部分值进行对比，如果不一致就抛异常
     *
     * 注意：如果jwt令牌解析失败一定会抛出异常（令牌过期、令牌篡改）
     */
    @Test
    public void testJwtParse(){
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJlNDMzNWQxNi00NWZlLTQ5YTItOTc3Ni00MjliM2VjMjZjNGQiLCJpYXQiOjE2MjMyOTUwOTIsImV4cCI6MTYyMzI5ODY5Miwic3ViIjoiY2xpZW50IiwidXNlcm5hbWUiOiJ6aGFuZ3NhbiIsImFnZSI6MjB9.euH2FhyLIxCbgxajxBt31xmxbNSKjIhMs1lKTSAZfHuwVPRmNJQyvT7UXNkduL1nAyr_GMmSGYWljt-3AyhGYQ";
        Jws<Claims> claimsJws = Jwts.parser().setSigningKey("itcast").parseClaimsJws(token);
        System.out.println("头：" + claimsJws.getHeader());
        System.out.println("载荷：" + claimsJws.getBody() );
        System.out.println("签名：" + claimsJws.getSignature());
    }
}
