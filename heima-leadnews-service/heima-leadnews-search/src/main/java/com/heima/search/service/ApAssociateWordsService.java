package com.heima.search.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.model.search.pojos.ApAssociateWords;

public interface ApAssociateWordsService extends IService<ApAssociateWords> {

    /**
     * 关键字联想词列表查询
     * @param dto
     * @return
     */
    ResponseResult search(UserSearchDto dto);


    /**
     * 关键字联想词列表查询（redis+trie数优化版）
     * @param dto
     * @return
     */
    ResponseResult searchV2(UserSearchDto dto);
}
