package com.heima.search.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.model.search.pojos.ApUserSearch;

public interface ApUserSearchService extends IService<ApUserSearch> {

    /**
     * 保存搜索记录
     * @param entryId 行为实体ID
     * @param keyword 搜索关键词
     */
    void insert(Integer entryId,String keyword);


    /**
     * 搜索记录列表查询
     * @param dto
     * @return
     */
    ResponseResult load(UserSearchDto dto);


    /**
     * 删除搜索记录
     * @param dto
     * @return
     */
    ResponseResult del(UserSearchDto dto);

}
