package com.heima.search.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.search.pojos.ApAssociateWords;

public interface ApAssociateWordsMapper  extends BaseMapper<ApAssociateWords> {
}
