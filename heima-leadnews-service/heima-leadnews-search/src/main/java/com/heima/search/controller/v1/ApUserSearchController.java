package com.heima.search.controller.v1;


import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.search.service.ApUserSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/history")
public class ApUserSearchController {

    @Autowired
    private ApUserSearchService apUserSearchService;


    @PostMapping("/load")
    public ResponseResult load(@RequestBody UserSearchDto dto){
        return apUserSearchService.load(dto);
    }



    @PostMapping("/del")
    public ResponseResult del(@RequestBody UserSearchDto dto){
        return apUserSearchService.del(dto);
    }
}
