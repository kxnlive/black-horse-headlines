package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;

public interface ArticleSearchService {


    /**
     * 关键词搜索
     * @param dto
     * @return
     */
    ResponseResult search(UserSearchDto dto);
}
