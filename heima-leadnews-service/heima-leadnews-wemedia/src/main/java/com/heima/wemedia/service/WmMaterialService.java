package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.springframework.web.multipart.MultipartFile;

public interface WmMaterialService extends IService<WmMaterial> {

    /**
     * 素材上传
     * @param multipartFile
     * @return
     */
    ResponseResult upload(MultipartFile multipartFile);


    /**
     * 素材列表分页查询
     * @param dto
     * @return
     */
    ResponseResult list(WmMaterialDto dto);


    /**
     * 根据素材ID删除素材
     * @param id
     * @return
     */
    ResponseResult del(Integer id);


    /**
     * 收藏或取消收藏
     * @param id
     * @param isCollection  0-取消收藏   1-收藏
     * @return
     */
    ResponseResult collect(Integer id, Short isCollection);
}
