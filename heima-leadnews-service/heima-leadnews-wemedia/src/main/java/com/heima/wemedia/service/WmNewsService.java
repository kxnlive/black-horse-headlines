package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmNewsDto;

import java.util.List;

public interface WmNewsService extends IService<WmNews> {


    /**
     * 自媒体文章分页列表查询
     * @param dto
     * @return
     */
    ResponseResult list(WmNewsPageReqDto dto);


    /**
     * 保存草稿或提交审核
     * @param dto
     * @param isSubmit  0-保存草稿  1-提交审核
     * @return
     */
    ResponseResult submit(WmNewsDto dto,Short isSubmit);


    /**
     * 查询文章详情
     * @param id
     * @return
     */
    ResponseResult findOne(Integer id);

    /**
     * 删除文章
     * @param id
     * @return
     */
    ResponseResult delOne(Integer id);


    /**
     * 上下架文章
     * @param dto
     * @return
     */
    ResponseResult downOrUp(WmNewsDto dto);


    /**
     * 查询审核通过待发布的文章
     * @return
     */
    List<Integer> findRelease();


    /**
     * 自媒体文章列表查询
     * @param dto
     * @return
     */
    ResponseResult listVo(NewsAuthDto dto);


    /**
     * 查询文章详情
     * @param id
     * @return
     */
    ResponseResult oneVo(Integer id);


    /**
     * 人工审核自媒体文章
     * @param dto
     * @param status 2-驳回， 4-通过
     * @return
     */
    ResponseResult auth(NewsAuthDto dto,Short status);

}
