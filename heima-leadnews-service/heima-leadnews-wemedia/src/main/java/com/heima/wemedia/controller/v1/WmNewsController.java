package com.heima.wemedia.controller.v1;


import com.heima.common.constants.wemedia.WemediaConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmNewsDto;
import com.heima.wemedia.service.WmNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/news/")
public class WmNewsController {

    @Autowired
    private WmNewsService wmNewsService;


    @PostMapping("/list")
    public ResponseResult list(@RequestBody WmNewsPageReqDto dto){
        return wmNewsService.list(dto);
    }


    @PostMapping("/submit")
    public ResponseResult submit(@RequestBody WmNewsDto dto){
        if(dto.getStatus().equals(WmNews.Status.NORMAL.getCode())){
            //保存草稿
            return wmNewsService.submit(dto, WmNews.Status.NORMAL.getCode());
        } else {
            //提交审核
            return wmNewsService.submit(dto, WmNews.Status.SUBMIT.getCode());
        }
    }


    @GetMapping("/one/{id}")
    public ResponseResult findOne(@PathVariable("id") Integer id){
        return wmNewsService.findOne(id);
    }


    @GetMapping("/del_news/{id}")
    public ResponseResult delOne(@PathVariable("id") Integer id){
        return wmNewsService.delOne(id);
    }


    @PostMapping("/down_or_up")
    public ResponseResult downOrUp(@RequestBody WmNewsDto dto){
        return wmNewsService.downOrUp(dto);
    }


    @PostMapping("/list_vo")
    public ResponseResult listVo(@RequestBody NewsAuthDto dto){
        return wmNewsService.listVo(dto);
    }


    @GetMapping("/one_vo/{id}")
    public ResponseResult oneVo(@PathVariable("id") Integer id){
        return wmNewsService.oneVo(id);
    }

    @PostMapping("/auth_pass")
    public ResponseResult authPass(@RequestBody NewsAuthDto dto){
        return wmNewsService.auth(dto, WemediaConstants.WM_NEWS_AUTH_PASS);
    }

    @PostMapping("/auth_fail")
    public ResponseResult authFail(@RequestBody NewsAuthDto dto){
        return wmNewsService.auth(dto, WemediaConstants.WM_NEWS_AUTH_FAIL);
    }

}
