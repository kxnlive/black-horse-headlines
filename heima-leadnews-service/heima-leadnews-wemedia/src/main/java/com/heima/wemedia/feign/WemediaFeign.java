package com.heima.wemedia.feign;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.apis.wemedia.IWemediaFeign;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.wemedia.service.WmNewsService;
import com.heima.wemedia.service.WmUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 这里虽然命名为feign，但本质上依然是controller，只是用于区分其他的非feign相关的controller
 */
@RestController
public class WemediaFeign implements IWemediaFeign {

    @Autowired
    private WmUserService wmUserService;


    @Autowired
    private WmNewsService wmNewsService;

    @Override
    @GetMapping("/api/v1/user/findByName/{name}")
    public WmUser findByName(@PathVariable("name") String name) {
        return wmUserService.getOne(Wrappers.<WmUser>lambdaQuery().eq(WmUser::getName,name));
    }


    @Override
    @PostMapping("/api/v1/user/save")
    public ResponseResult save(@RequestBody WmUser wmUser){
        wmUserService.save(wmUser);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    @GetMapping("/api/v1/news/findOne/{id}")
    public WmNews findOneWmNews(@PathVariable("id") Integer id) {
        return wmNewsService.getById(id);
    }

    @Override
    @PostMapping("/api/v1/news/update")
    public ResponseResult updateWmNews(@RequestBody WmNews wmNews) {
        wmNewsService.updateById(wmNews);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


    @Override
    @GetMapping("/api/v1/news/findRelease")
    public List<Integer> findRelease(){
        return wmNewsService.findRelease();
    }
}
