package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WmNewsMaterialMapper extends BaseMapper<WmNewsMaterial> {


    /**
     * 保存素材与文章的关系
     * @param materialIdList 素材ID列表
     * @param newId 文章ID
     * @param type 引用类型，0-正文引用  1-封面引用
     */
    void saveRelationsByContent(@Param("materialIdList") List<Integer> materialIdList, @Param("newsId") Integer newId, @Param("type") int type);
}