package com.heima.wemedia.test;

import com.heima.file.service.FileStorageService;
import com.heima.wemedia.WemediaApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@SpringBootTest(classes = WemediaApplication.class)
@RunWith(SpringRunner.class)
public class OSSTest {


    @Autowired
    private FileStorageService fileStorageService;

    @Value("${file.oss.prefix}")
    private String prefix; //资源在OSS上的顶层目录

    @Value("${file.oss.web-site}")
    private String webSite; //访问阿里云OSS资源的域名


    /**
     * 测试文件上传
     */
    @Test
    public void testUpload() throws FileNotFoundException {

        InputStream inputStream = new FileInputStream("C:\\Users\\wujintao\\Desktop\\图片\\4.png");

        String path = fileStorageService.store(prefix, "bee.png", inputStream); //上传后获取到的结果是文件在OSS上的相对路径

        String fileUrl = webSite + path;

        System.out.println(fileUrl);
    }


    /**
     * 测试文件删除
     */
    @Test
    public void testDelete(){
        fileStorageService.delete("material/2021/6/20210613/bee.png");
    }
}
