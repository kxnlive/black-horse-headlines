package com.heima.article.controller.v1;


import com.heima.article.service.ApBehaviorRelationService;
import com.heima.model.article.dtos.ArticleInfoDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/article/load_article_behavior")
public class ApBehaviorRelationController {

    @Autowired
    private ApBehaviorRelationService apBehaviorRelationService;


    @PostMapping
    public ResponseResult load(@RequestBody ArticleInfoDto dto){
        return apBehaviorRelationService.load(dto);
    }
}
