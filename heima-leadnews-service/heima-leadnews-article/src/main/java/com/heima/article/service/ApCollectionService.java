package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.behavior.dtos.CollectionBehaviorDto;
import com.heima.model.behavior.pojos.ApCollection;
import com.heima.model.common.dtos.ResponseResult;

public interface ApCollectionService extends IService<ApCollection> {

    /**
     * 收藏
     * @param dto
     * @return
     */
    ResponseResult collection(CollectionBehaviorDto dto);
}
