package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.behavior.pojos.ApCollection;

public interface ApCollectionMapper extends BaseMapper<ApCollection> {
}
