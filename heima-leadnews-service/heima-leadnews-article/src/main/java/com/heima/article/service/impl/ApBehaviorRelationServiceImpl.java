package com.heima.article.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.apis.behavior.IBehaviorFeign;
import com.heima.apis.user.IUserFeign;
import com.heima.article.service.ApAuthorService;
import com.heima.article.service.ApBehaviorRelationService;
import com.heima.article.service.ApCollectionService;
import com.heima.model.article.dtos.ArticleInfoDto;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApCollection;
import com.heima.model.behavior.pojos.ApLikesBehavior;
import com.heima.model.behavior.pojos.ApUnlikesBehavior;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.model.user.pojos.ApUserFollow;
import com.heima.utils.threadlocal.ApThreadLocalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Transactional
@Service
public class ApBehaviorRelationServiceImpl implements ApBehaviorRelationService {
    @Autowired
    private IBehaviorFeign behaviorFeign;

    @Autowired
    private IUserFeign userFeign;

    @Autowired
    private ApCollectionService apCollectionService;

    @Autowired
    private ApAuthorService apAuthorService;


    @Override
    public ResponseResult load(ArticleInfoDto dto) {
        //1.判断参数
        if(dto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        ApUser apUser = ApThreadLocalUtils.getUser();

        Integer userId = null;
        if(apUser.getId()!=null && apUser.getId()>0){
            userId = apUser.getId();
        }

        //2.调用feign查询行为实体判断是否存在
        ApBehaviorEntry apBehaviorEntry = behaviorFeign.findByUserIdAndEquipmentId(userId, dto.getEquipmentId());
        if(apBehaviorEntry==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "行为实体不存在");
        }

        boolean isunlike=false, islike=false, iscollection=false, isfollow=false;

        //3.调用feign查询不喜欢行为
        ApUnlikesBehavior apUnlikesBehavior = behaviorFeign.findOneUnlikesBehavior(dto.getArticleId(), apBehaviorEntry.getId());
        //如果不喜欢行为存在，且当前类型的确是不喜欢
        if(apUnlikesBehavior!=null && apUnlikesBehavior.getType().equals(ApUnlikesBehavior.Type.UNLIKE.getCode())){
            isunlike = true;
        }

        //4.调用feign查询点赞行为
        ApLikesBehavior apLikesBehavior = behaviorFeign.findOneLikesBehavior(dto.getArticleId(), apBehaviorEntry.getId(),ApLikesBehavior.Type.ARTICLE.getCode());
        if(apLikesBehavior!=null && apLikesBehavior.getOperation().equals(ApLikesBehavior.Operation.LIKE.getCode())){
            islike = true;
        }


        //5.本地查询收藏行为
        ApCollection apCollection = apCollectionService.getOne(Wrappers.<ApCollection>lambdaQuery().
                eq(ApCollection::getEntryId, apBehaviorEntry.getId()).
                eq(ApCollection::getArticleId, dto.getArticleId()).
                eq(ApCollection::getType, ApCollection.Type.ARTICLE.getCode()));
        if(apCollection!=null){
            iscollection = true;
        }


        //6.调用feign查询关注行为

        ApAuthor apAuthor = apAuthorService.getById(dto.getAuthorId());

        ApUserFollow userFollow = userFeign.findOneUserFollow(userId, apAuthor.getUserId());
        if(userFollow!=null){
            isfollow = true;
        }

        //7.封装数据并响应
        Map map = new HashMap<>();
        map.put("isunlike",isunlike); //是否不喜欢
        map.put("islike", islike); //是否点赞
        map.put("iscollection", iscollection); //是否收藏
        map.put("isfollow", isfollow); //是否关注

        return ResponseResult.okResult(map);
    }
}
