package com.heima.article.feign;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.apis.article.IArticleFeign;
import com.heima.article.service.ApArticleService;
import com.heima.article.service.ApAuthorService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.pojos.ApAuthor;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ArticleFeign implements IArticleFeign {

    @Autowired
    private ApAuthorService apAuthorService;

    @Override
    @GetMapping("/api/v1/author/findByUserId/{userId}")
    public ApAuthor findByUserId(@PathVariable("userId") Integer userId) {
        return apAuthorService.getOne(Wrappers.<ApAuthor>lambdaQuery().eq(ApAuthor::getUserId, userId));
    }

    @Override
    @PostMapping("/api/v1/author/save")
    public ResponseResult save(@RequestBody ApAuthor apAuthor) {
        apAuthorService.save(apAuthor);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


    @Override
    public ApAuthor findByWmUserId(Integer wmUserId) {
        return apAuthorService.getOne(Wrappers.<ApAuthor>lambdaQuery().eq(ApAuthor::getWmUserId, wmUserId));
    }

    @Autowired
    private ApArticleService apArticleService;

    @Override
    public ResponseResult saveApArticle(ArticleDto dto) {
        return apArticleService.saveApArticle(dto);
    }


    @Override
    @GetMapping("/api/v1/author/one/{id}")
    public ApAuthor findAuthorById(@PathVariable("id") Integer id) {
        return apAuthorService.getById(id);
    }
}
