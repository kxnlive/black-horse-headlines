package com.heima.article.service;

import com.heima.model.article.mess.ArticleVisitStreamMess;

public interface HotArticleService {

    /**
     * 计算文章分值
     */
    void computeScore();


    /**
     * 重新计算文章分值
     */
    void recomputeScore(ArticleVisitStreamMess articleVisitStreamMess);
}
