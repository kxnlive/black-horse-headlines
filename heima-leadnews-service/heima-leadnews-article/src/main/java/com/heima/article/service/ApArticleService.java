package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.dtos.ArticleInfoDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;

public interface ApArticleService extends IService<ApArticle> {

    /**
     * 保存或更新文章
     * @param dto
     * @return
     */
    ResponseResult saveApArticle(ArticleDto dto);


    /**
     * 加载更多或更新
     * @param dto
     * @param type 1-加载更多 2-加载更新
     * @return
     */
    ResponseResult load(ArticleHomeDto dto,Short type);

    /**
     * 加载更多或更新（redis版本）
     * @param dto
     * @param type 1-加载更多 2-加载更新
     * @param  isFromIndex 请求是否来自于首页
     * @return
     */
    ResponseResult load(ArticleHomeDto dto,Short type, Boolean isFromIndex);


    /***
     * 根据ID查询文章详情
     * @param dto
     * @return
     */
    ResponseResult loadInfo(ArticleInfoDto dto);
}