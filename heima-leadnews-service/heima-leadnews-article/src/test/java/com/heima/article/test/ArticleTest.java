package com.heima.article.test;


import com.heima.article.ArticleApplication;
import com.heima.article.service.ApArticleService;
import com.heima.model.article.pojos.ApArticle;
import org.apache.commons.collections.map.HashedMap;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 文章单元测试
 */
@SpringBootTest(classes = ArticleApplication.class)
@RunWith(SpringRunner.class)
public class ArticleTest {

    @Autowired
    private RestHighLevelClient restHighLevelClient;
    
    @Autowired
    private ApArticleService apArticleService;

    /**
     * 导入全部文章数据到ES
     */
    @Test
    public void testImportToES(){
        
        //1.查询全部文章数据列表
        List<ApArticle> apArticleList = apArticleService.list();


        //2.遍历将每条文章导入到ES
        for (ApArticle apArticle : apArticleList) {

            Map articleMap = new HashedMap(); //每一条要导入到ES中的文章
            articleMap.put("publishTime", apArticle.getPublishTime());//发布时间
            articleMap.put("layout", apArticle.getLayout());//布局方式
            articleMap.put("images", apArticle.getImages());//封面图片
            articleMap.put("authorId", apArticle.getAuthorId());//作者ID
            articleMap.put("title", apArticle.getTitle());//文章标题


            IndexRequest indexRequest = new IndexRequest("app_info_article");
            indexRequest.source(articleMap).id(String.valueOf(apArticle.getId()));

            try {
                IndexResponse indexResponse = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
                System.out.println(indexResponse.getResult().getLowercase()); //created
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
