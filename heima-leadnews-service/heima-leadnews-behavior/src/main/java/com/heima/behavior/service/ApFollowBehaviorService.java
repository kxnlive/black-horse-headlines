package com.heima.behavior.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.behavior.dtos.FollowBehaviorDto;
import com.heima.model.behavior.pojos.ApFollowBehavior;

public interface ApFollowBehaviorService extends IService<ApFollowBehavior> {


    /**
     * 关注行为或取消关注行为
     * @param dto
     */
    void follow(FollowBehaviorDto dto);

}
