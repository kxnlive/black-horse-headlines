package com.heima.behavior.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.behavior.dtos.ReadBehaviorDto;
import com.heima.model.behavior.pojos.ApReadBehavior;
import com.heima.model.common.dtos.ResponseResult;


public interface ApReadBehaviorService extends IService<ApReadBehavior> {


    /**
     * 阅读行为处理
     * @param dto
     * @return
     */
    ResponseResult read(ReadBehaviorDto dto);
}
