package com.heima.behavior.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.behavior.mapper.ApUnlikesBehaviorMapper;
import com.heima.behavior.service.ApBehaviorEntryService;
import com.heima.behavior.service.ApUnlikesBehaviorService;
import com.heima.model.behavior.dtos.UnLikesBehaviorDto;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApUnlikesBehavior;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.ApThreadLocalUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * APP不喜欢行为表 服务实现类
 * </p>
 *
 * @author itheima
 */
@Slf4j
@Service
public class ApUnlikesBehaviorServiceImpl extends ServiceImpl<ApUnlikesBehaviorMapper, ApUnlikesBehavior> implements ApUnlikesBehaviorService {
    @Autowired
    private ApBehaviorEntryService apBehaviorEntryService;

    @Override
    public ResponseResult unlike(UnLikesBehaviorDto dto) {
        //1.检查参数
        if(dto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //2.获取用户ID
        ApUser apUser = ApThreadLocalUtils.getUser();
        Integer userId = null;
        if(apUser!=null && apUser.getId()!=0){
            userId = apUser.getId();//获取正常登录用户的ID值
        }

        //3.根据用户ID和设备ID查询行为实体并判断是否存在
        ApBehaviorEntry apBehaviorEntry = apBehaviorEntryService.findByUserIdAndEquipmentId(userId, dto.getEquipmentId());
        if(apBehaviorEntry==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "行为实体不存在");
        }

        //4.查询不喜欢行为数据判断是否存在，如果不存在则新增
        ApUnlikesBehavior apUnlikesBehavior = getOne(Wrappers.<ApUnlikesBehavior>lambdaQuery().eq(ApUnlikesBehavior::getEntryId, apBehaviorEntry.getId()).eq(ApUnlikesBehavior::getArticleId, dto.getArticleId()));
        if(apUnlikesBehavior==null){

            if(dto.getType().equals(ApUnlikesBehavior.Type.CANCEL.getCode())){
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "数据不存在无法取消不喜欢");
            }

            apUnlikesBehavior = new ApUnlikesBehavior();
            apUnlikesBehavior.setEntryId(apBehaviorEntry.getId()); //行为实体ID
            apUnlikesBehavior.setArticleId(dto.getArticleId());//文章ID
            apUnlikesBehavior.setType(dto.getType()); //0-不喜欢，1-取消不喜欢
            apUnlikesBehavior.setCreatedTime(new Date());//创建时间
            save(apUnlikesBehavior);
        } else { //5.如果存在则更新

            if(dto.getType().equals(ApUnlikesBehavior.Type.CANCEL.getCode()) && apUnlikesBehavior.getType().equals(ApUnlikesBehavior.Type.CANCEL.getCode())){
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "不能重复点击取消不喜欢");
            }

            if(dto.getType().equals(ApUnlikesBehavior.Type.UNLIKE.getCode()) && apUnlikesBehavior.getType().equals(ApUnlikesBehavior.Type.UNLIKE.getCode())){
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "不能重复点击不喜欢");
            }

            ApUnlikesBehavior apUnlikesBehaviorDB = new ApUnlikesBehavior();
            apUnlikesBehaviorDB.setId(apUnlikesBehavior.getId());
            apUnlikesBehaviorDB.setType(dto.getType());
            updateById(apUnlikesBehaviorDB);
        }

        //6.响应数据
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}