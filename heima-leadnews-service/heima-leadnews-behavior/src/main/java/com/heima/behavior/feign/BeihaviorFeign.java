package com.heima.behavior.feign;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.apis.behavior.IBehaviorFeign;
import com.heima.behavior.service.ApBehaviorEntryService;
import com.heima.behavior.service.ApLikesBehaviorService;
import com.heima.behavior.service.ApUnlikesBehaviorService;
import com.heima.model.behavior.pojos.ApBehaviorEntry;
import com.heima.model.behavior.pojos.ApLikesBehavior;
import com.heima.model.behavior.pojos.ApUnlikesBehavior;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BeihaviorFeign implements IBehaviorFeign {

    @Autowired
    private ApBehaviorEntryService apBehaviorEntryService;

    @GetMapping("/api/v1/findByUserIdAndEquipmentId")
    public ApBehaviorEntry findByUserIdAndEquipmentId(@RequestParam(value = "userId", required = false) Integer userId,
                                               @RequestParam(value = "equipmentId", required = false) Integer equipmentId){
        return  apBehaviorEntryService.findByUserIdAndEquipmentId(userId, equipmentId);
    }

    @Autowired
    private ApLikesBehaviorService apLikesBehaviorService;

    @Override
    @GetMapping("/api/v1/likes_behavior/one")
    public ApLikesBehavior findOneLikesBehavior(@RequestParam Long articleId,@RequestParam Integer entryId,@RequestParam Short type) {
        return apLikesBehaviorService.getOne(Wrappers.<ApLikesBehavior>lambdaQuery()
                .eq(ApLikesBehavior::getEntryId, entryId)
                .eq(ApLikesBehavior::getArticleId, articleId)
                .eq(ApLikesBehavior::getType, type));
    }

    @Autowired
    private ApUnlikesBehaviorService apUnlikesBehaviorService;

    @Override
    @GetMapping("/api/v1/un_likes_behavior/one")
    public ApUnlikesBehavior findOneUnlikesBehavior(@RequestParam Long articleId,@RequestParam Integer entryId) {
        return apUnlikesBehaviorService.getOne(Wrappers.<ApUnlikesBehavior>lambdaQuery()
                .eq(ApUnlikesBehavior::getEntryId, entryId)
                .eq(ApUnlikesBehavior::getArticleId, articleId));
    }
}
