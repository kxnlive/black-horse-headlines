package com.heima.common.exception;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//@ControllerAdvice
@RestControllerAdvice
@Slf4j
public class ExceptionCatch {


    /**
     * 统一捕获不可预知异常
     * @param e
     * @return
     */
    //@ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ResponseResult catchException(Exception e){
        e.printStackTrace();
        log.error("不可预知异常：{}", e.getMessage());
        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
    }


    /**
     * 统一捕获可可预知异常
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = CustomException.class)
    public ResponseResult catchException(CustomException e){
        e.printStackTrace();
        log.error("可预知异常：{}", e.getMessage());
        return ResponseResult.errorResult(e.getAppHttpCodeEnum());
    }
}
