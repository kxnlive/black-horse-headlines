package com.heima.common.constants.article;

public class HotArticleConstants {

    /**
     * 入口topic
     */
    public static final String HOT_ARTICLE_SCORE_TOPIC="hot.article.score.topic";


    /**
     * 出口topic
     */
    public static final String HOT_ARTICLE_INCR_HANDLE_TOPIC="hot.article.incr.handle.topic";

}