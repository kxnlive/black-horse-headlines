package com.heima.common.constants.article;

public class ArticleConstants {
    public static final Short LOADTYPE_LOAD_MORE = 1; //加载更多
    public static final Short LOADTYPE_LOAD_NEW = 2; //加载更新
    public static final String DEFAULT_TAG = "__all__"; //全部频道

    public static final Integer HOT_ARTICLE_LIKE_WEIGHT = 3; //点赞权重
    public static final Integer HOT_ARTICLE_COMMENT_WEIGHT = 5; //评论权重
    public static final Integer HOT_ARTICLE_COLLECTION_WEIGHT = 8; //收藏权重

    public static final String HOT_ARTICLE_FIRST_PAGE = "hot_article_first_page_";
}